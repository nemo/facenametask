function DisconnectTI(stimulator)
% Disconnect if connected
t_pause=2;
% reset loads
disp(['** serial out: ','ChConfig ','00',' ','00',' ','00',' ','00',' ','08',' ','09',' ','10',' ','11']);
fprintf(stimulator,['ChConfig ','00',' ','00',' ','00',' ','00',' ','08',' ','09',' ','10',' ','11']);
% close connection
fclose(stimulator);
pause(t_pause)
% check connecttion
status = stimulator.Status;
if  strcmp(status,'closed')
    disp('* TI stimulator was disconnected.');
    connectStatus = 0;
else
    disp('* TI stimulator is still connected.');
end

end
