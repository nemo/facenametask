function [SubjectInfo] = GetSubjectInfo(ResultsDir)

%% Get Subject information
% % prompt = {'Subject number:', 'Initials:', 'Date of birth:', 'Handedness:', 'Block:', 'Run:', 'Session:', 'Stimulation (true/false)', 'COM PORT', 'MRI (true / false)'}; %description of fields
% % defaults = {'','','','right','','1','1','','','false'};
% % options.Resize='on';
% % infoTask = inputdlg(prompt, 'Face Name Task',1,defaults,options); %opens dialog
% % SUBJECT = infoTask{1,:}; %Gets Subject Number
% % Block = infoTask{5,:};
% % Run = infoTask{6,:};
% % Session = infoTask{7,:};
% % Stimulation = infoTask{8,:};
% % COMport = infoTask{9,:};
% % MRI = infoTask{end,:};

Title = 'Face Name Task';

%%%% SETTING DIALOG OPTIONS
% Options.WindowStyle = 'modal';
Options.Resize = 'on';
Options.Interpreter = 'tex';
Options.CancelButton = 'on';
Options.ApplyButton = 'off';
Options.ButtonNames = {'Continue','Cancel'}; %<- default names, included here just for illustration
Option.Dim = 8; % Horizontal dimension in fields

Prompt = {};
Formats = {};
DefAns = struct([]);

% Prompt(1,:) = {['Face Name Task'],[],[]};
% Formats(1,1).type = 'text';
% Formats(1,1).size = [-1 0];
% Formats(1,1).span = [1 2]; % item is 1 field x 4 fields

Prompt(1,:) = {'Subject Number', 'Number',[]};
Formats(1,1).type = 'edit';
Formats(1,1).format = 'text';
Formats(1,1).size = 30; % automatically assign the height
DefAns(1).Number = '';


Prompt(2,:) = {'Initials', 'Initials',[]};
Formats(2,1).type = 'edit';
Formats(2,1).format = 'text';
Formats(2,1).size = 80;
DefAns.Initials = '';

Prompt(3,:) = {'Block Order', 'Block','(no space or hyphen, 1 to 9)'};
Formats(3,1).type = 'edit';
Formats(3,1).format = 'integer';
Formats(3,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(3,1).size = 80;
Formats(3,1).unitsloc = 'bottomleft';
DefAns.Block = 123456789;

Prompt(4,:) = {'Run', 'Run','(run number)'};
Formats(4,1).type = 'edit';
Formats(4,1).format = 'integer';
Formats(4,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(4,1).size = 80;
Formats(4,1).unitsloc = 'bottomleft';
DefAns.Run = 1;


Prompt(5,:) = {'Session', 'Session','(session number)'};
Formats(5,1).type = 'edit';
Formats(5,1).format = 'integer';
Formats(5,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(5,1).size = 80;
Formats(5,1).unitsloc = 'bottomleft';
DefAns.Session = '';

Prompt(end+1,:) = {'Stimulation','Stimulation',[]};
Formats(6,1).type = 'list';
Formats(6,1).format = 'text';
Formats(6,1).style = 'radiobutton';
Formats(6,1).items = {'Yes' 'No'};
% Formats(7,1).span = [2 1];  % item is 2 field x 1 fields
DefAns.Stimulation = 'No';%3; % 

Prompt(end+1,:) = {'COM Port','Port','(port number - for Stim)'};
Formats(7,1).type = 'edit';
Formats(7,1).format = 'integer';
Formats(7,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(7,1).size = 80;
Formats(7,1).unitsloc = 'bottomleft';
DefAns.Port = 5;
 
Prompt(end+1,:) = {'MRI','MRI',[]};
Formats(8,1).type = 'list';
Formats(8,1).format = 'text';
Formats(8,1).style = 'radiobutton';
Formats(8,1).items = {'No' 'CIF' 'Invicro'};
% Formats(7,1).span = [2 1];  % item is 2 field x 1 fields
DefAns.MRI = 'No';%3; % 



[Answer,Cancelled] = inputsdlg(Prompt,Title,Formats,DefAns,Options)

SUBJECT = Answer.Number;
Session = Answer.Session;
Run = Answer.Run;
Block = Answer.Block;

cTask = clock; %Current date and time as date vector. [year month day hour minute seconds]
baseNameTask=[SUBJECT '_S_' Session '_Block_' Block '_FaceName_' num2str(cTask(3)) '_' num2str(cTask(2)) '_' num2str(cTask(1)) '_' num2str(cTask(4)) '_' num2str(cTask(5))]; %makes unique filename

%% Create directory to save Data
SubjFolder = ([ResultsDir filesep 'Subject_' char(SUBJECT)]);

% Create Subject folder
if ~exist(SubjFolder, 'dir')
    mkdir(SubjFolder);
end

% Create Parameters folder
ParamFolder = ([SubjFolder filesep 'Parameters' ]);
if ~exist(ParamFolder, 'dir')
    mkdir(ParamFolder);
end

% Create session folder
SessionFolder = ([SubjFolder filesep 'Session_' char(Session)]);
if ~exist(SessionFolder, 'dir')
    mkdir(SessionFolder);
end

% Create Run folder
RunFolder = ([SessionFolder filesep 'Run_' char(Run)]);
if ~exist(RunFolder, 'dir')
    mkdir(RunFolder);
end


%SubjectInfo.infoTask = infoTask;
SubjectInfo.Subject = SUBJECT;
SubjectInfo.Session = Session;
SubjectInfo.Block = Block;
SubjectInfo.Run = Run;
SubjectInfo.cTask = cTask;
SubjectInfo.baseNameTask = baseNameTask;
SubjectInfo.SubjFolder = SubjFolder;
SubjectInfo.ParamFolder = ParamFolder;
SubjectInfo.SessionFolder = SessionFolder;
SubjectInfo.RunFolder = RunFolder;


if strcmp(Answer.Stimulation,'Yes'); Stimulation = true; end
if strcmp(Answer.Stimulation,'No'); Stimulation = false; end

if strcmp(Answer.MRI,'No'); MRI = false; end
if strcmp(Answer.MRI,'CIF'); MRI = true; end
if strcmp(Answer.MRI,'Invicro'); MRI = true; end

SubjectInfo.Stimulation = Stimulation;
SubjectInfo.COM = (['COM' num2str(Answer.Port)]);
SubjectInfo.MRI = MRI;


if (Stimulation)  
    port = SubjectInfo.COM;
    % Standard Parameters to start with
    
    rampOnTime = 5*1000;        % ms
    rampOffTime =5*1000;        % ms
    
    % Channel 1
    Freq1 = 2000;          % Hz
    Amp1 = 2;             % mA
    load1a = '20';
    load1b = '20';
    pin1a = '01';
    pin1b = '02';
    
    % Channel 2
    Freq2 = 2005;          % Hz
    Amp2 = 2;              % mA
    load2a = '20';
    load2b = '20';
    pin2a = '03';
    pin2b = '04';
    % Initialise TI
    [stimulator] = InitialiseTI(port);
    % Give some initial parameters
    UploadStimTI(stimulator, Freq1, Freq2, Amp1, Amp2, load1a, load1b, load2a, load2b, pin1a, pin1b, pin2a, pin2b)
end


end