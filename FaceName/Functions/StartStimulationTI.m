function StartStimulationTI(stimulator, Amp1, Amp2, rampOnTime)
disp('* Stimulating...');
fprintf(stimulator,['RampWfm mA 0 ',num2str(Amp1),' 0 ',num2str(Amp2),' ',num2str(rampOnTime)]);
%disp(['** serial out: ','RampWfm mA 0 ',num2str(Amp1),' 0 ',num2str(Amp2),' ',num2str(rampOnTime)]);
end