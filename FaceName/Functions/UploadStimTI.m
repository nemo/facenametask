function UploadStimTI(stimulator, Freq1, Freq2, Amp1, Amp2, load1a, load1b, load2a, load2b, pin1a, pin1b, pin2a, pin2b)

    %% Upload stimulation parameters
    t_pause = 2;
    % Frequencies
    disp('* Uploading device parameters');
    disp(['** serial out: ','DDS 0 ', num2str(Freq1)]);
    pause(t_pause)
    fprintf(stimulator,['DDS 0 ', num2str(Freq1)]);
    disp(['** serial out: ','DDS 1 ', num2str(Freq2)]);
    pause(t_pause)
    fprintf(stimulator,['DDS 1 ', num2str(Freq2)]);
    % Electrodes & Pins
    disp(['** serial out: ','ChConfig ',load1a,' ',load1b,' ',load2a,' ',load2b,' ',pin1a,' ',pin1b,' ',pin2a,' ',pin2b]);
    pause(t_pause)
    fprintf(stimulator,['ChConfig ',load1a,' ',load1b,' ',load2a,' ',load2b,' ',pin1a,' ',pin1b,' ',pin2a,' ',pin2b]);
end
