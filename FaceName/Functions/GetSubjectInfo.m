function [SubjectInfo, TaskParameters, OrderStim, StructBlockParameters] = GetSubjectInfo(ResultsDir)


Title = 'Face Name Task';

%%%% SETTING DIALOG OPTIONS
% Options.WindowStyle = 'modal';
Options.Resize = 'on';
Options.Interpreter = 'tex';
Options.CancelButton = 'on';
Options.ApplyButton = 'off';
Options.ButtonNames = {'Continue','Cancel'}; %<- default names, included here just for illustration
Option.Dim = 8; % Horizontal dimension in fields

Prompt = {};
Formats = {};
DefAns = struct([]);

% Prompt(1,:) = {['Face Name Task'],[],[]};
% Formats(1,1).type = 'text';
% Formats(1,1).size = [-1 0];
% Formats(1,1).span = [1 2]; % item is 1 field x 4 fields

Prompt(1,:) = {'Subject Number', 'Number',[]};
Formats(1,1).type = 'edit';
Formats(1,1).format = 'text';
Formats(1,1).size = 30; % automatically assign the height
DefAns(1).Number = '';


Prompt(2,:) = {'Initials', 'Initials',[]};
Formats(2,1).type = 'edit';
Formats(2,1).format = 'text';
Formats(2,1).size = 80;
DefAns.Initials = '';


Prompt(3,:) = {'Task Parameters','Parameters',[]};
Formats(3,1).type = 'list';
Formats(3,1).format = 'text';
Formats(3,1).style = 'radiobutton';
Formats(3,1).items = { 'TIDES-MRI' 'TIDES-Behv' 'Part-the-Cloud'};
%Formats(9,1).items = {'Behv-Standard' 'TIDES-MRI' 'Part-the-Cloud' 'TIDES-Behv' 'Practice'};
% Formats(7,1).span = [2 1];  % item is 2 field x 1 fields
DefAns.Parameters = 'TIDES-Behv';%3; % 


Prompt(4,:) = {'Type','Type',[]};
Formats(4,1).type = 'list';
Formats(4,1).format = 'text';
Formats(4,1).style = 'radiobutton';
Formats(4,1).items = {'Task' 'Practice'};
DefAns.Type = 'Task';%3; % 


Prompt(5,:) = {'Session', 'Session','(session number)'};
Formats(5,1).type = 'edit';
Formats(5,1).format = 'integer';
Formats(5,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(5,1).size = 80;
Formats(5,1).unitsloc = 'bottomleft';
DefAns.Session = '';


Prompt(6,:) = {'Run', 'Run','(run number; use 0 for Practice)'};
Formats(6,1).type = 'edit';
Formats(6,1).format = 'integer';
Formats(6,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(6,1).size = 80;
Formats(6,1).unitsloc = 'bottomleft';
DefAns.Run = 1;


Prompt(7,:) = {'Block Order', 'Block','(use space between numbers (up to 9))'};
Formats(7,1).type = 'edit';
Formats(7,1).format = 'text';
Formats(7,1).size = 120;
Formats(7,1).unitsloc = 'bottomleft';
DefAns.Block = '1 2 3 4';


Prompt(end+1,:) = {'Task Stages','Stages',[]};
Formats(8,1).type = 'list';
Formats(8,1).format = 'text';
Formats(8,1).style = 'radiobutton';
Formats(8,1).items = {'Encode + Recall' 'Recall Only'};
DefAns.Stages = 'Encode + Recall';%3; % 


Prompt(end+1,:) = {'Stimulation','Stimulation',[]};
Formats(9,1).type = 'list';
Formats(9,1).format = 'text';
Formats(9,1).style = 'radiobutton';
Formats(9,1).items = {'Yes' 'No'};
% Formats(7,1).span = [2 1];  % item is 2 field x 1 fields
DefAns.Stimulation = 'Yes';%3; % 

Prompt(end+1,:) = {'COM Port','Port','(port number - for Stim)'};
Formats(10,1).type = 'edit';
Formats(10,1).format = 'integer';
Formats(10,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(10,1).size = 80;
Formats(10,1).unitsloc = 'bottomleft';
DefAns.Port = 5;
 
Prompt(end+1,:) = {'MRI','MRI',[]};
Formats(11,1).type = 'list';
Formats(11,1).format = 'text';
Formats(11,1).style = 'radiobutton';
Formats(11,1).items = {'No' 'CIF' 'Invicro'};
% Formats(7,1).span = [2 1];  % item is 2 field x 1 fields
DefAns.MRI = 'No';%3; % 


[Answer,Cancelled] = inputsdlg(Prompt,Title,Formats,DefAns,Options)

if (Cancelled)
    error(' ---- Cancel button pressed -----');
    return;
end


SUBJECT = Answer.Number;
Session = Answer.Session;
Run = Answer.Run;
Block = Answer.Block;

cTask = clock; %Current date and time as date vector. [year month day hour minute seconds]
baseNameTask=[SUBJECT '_S_' num2str(Session) '_Block_' num2str(Block) '_FaceName_' num2str(cTask(3)) '_' num2str(cTask(2)) '_' num2str(cTask(1)) '_' num2str(cTask(4)) '_' num2str(cTask(5))]; %makes unique filename

%% Create directory to save Data

%SubjectInfo.infoTask = infoTask;
SubjectInfo.Subject = SUBJECT;
SubjectInfo.Session = Session;
SubjectInfo.Block = Block;
SubjectInfo.Run = Run;
SubjectInfo.cTask = cTask;
SubjectInfo.baseNameTask = baseNameTask;
SubjectInfo.Study = Answer.Parameters;

SubjFolder = ([ResultsDir filesep 'Subject_' num2str(SUBJECT)]);

% Create Subject folder
if ~exist(SubjFolder, 'dir')
    mkdir(SubjFolder);
end

% Create Parameters folder

ParamFolder = ([SubjFolder filesep 'Parameters' ]);
if ~exist(ParamFolder, 'dir')
    mkdir(ParamFolder);
end
SubjectInfo.ParamFolder = ParamFolder;

% Create session folder
SessionFolder = ([SubjFolder filesep 'Session_' num2str(Session)]);
if ~exist(SessionFolder, 'dir')
    mkdir(SessionFolder);
end

% Create Run folder
RunFolder = ([SessionFolder filesep 'Run_' num2str(Run)]);
if ~exist(RunFolder, 'dir')
    mkdir(RunFolder);
end


SubjectInfo.SubjFolder = SubjFolder;

SubjectInfo.SessionFolder = SessionFolder;
SubjectInfo.RunFolder = RunFolder;


if strcmp(Answer.Stimulation,'Yes'); Stimulation = true; end
if strcmp(Answer.Stimulation,'No'); Stimulation = false; end

if strcmp(Answer.MRI,'No'); MRI = false; end
if strcmp(Answer.MRI,'CIF'); MRI = true; end
if strcmp(Answer.MRI,'Invicro'); MRI = true; end

if strcmp(Answer.Stages,'Encode + Recall'); encode = true; end
if strcmp(Answer.Stages,'Recall Only'); encode = false; end



SubjectInfo.Stimulation = Stimulation;
SubjectInfo.COM = (['COM' num2str(Answer.Port)]);
SubjectInfo.MRI = MRI;
SubjectInfo.MRIsite = Answer.MRI;
SubjectInfo.Encode = encode;
SubjectInfo.Type = Answer.Type;

ppath=('Parameters');

%% Task Parameters
if strcmp(Answer.Parameters,'Behv-Standard');
    TaskParameters.FaceDisplayTime = 2;          % Time face is displayed
    TaskParameters.TimeResponse = 20;            % Max time allowed for a response
    TaskParameters.NumTrials = 16;               % Num of faces to learn per block - 4 per category: WM, WF, BM, BF
    TaskParameters.ISI = 0;                      % Inter-stimulus interval - in effect 1s, takes ~100 ms to load the picture
    TaskParameters.BaselineTime = 5;             % Duration of the baseline - 5s to match rump up and rump down - during rump down do we do a filler task (?)
    TaskParameters.NumTrialsResp = TaskParameters.NumTrials;
    TaskParameters.TimeConfidence = 4;
    TaskParameters.MaintainTime = TaskParameters.BaselineTime;
    load ([ppath filesep 'BlockParameters_3Cond.mat'])
end



if strcmp(Answer.Parameters,'TIDES-MRI');
    TaskParameters.FaceDisplayTime = 2;          % Time face is displayed
    TaskParameters.TimeResponse = 20;            % Max time allowed for a response
    TaskParameters.NumTrials = 16;               % Num of faces to learn per block - 4 per category: WM, WF, BM, BF
    TaskParameters.ISI = 0;                      % Inter-stimulus interval - in effect 1s, takes ~100 ms to load the picture
    TaskParameters.BaselineTime = 16;            % Duration of the baseline 
    TaskParameters.NumTrialsResp = TaskParameters.NumTrials;
    TaskParameters.TimeConfidence = 4;
    TaskParameters.MaintainTime = TaskParameters.BaselineTime;
    load ([ppath filesep 'BlockParameters_3Cond.mat'])

end

if strcmp(Answer.Parameters,'Part-the-Cloud');
    TaskParameters.FaceDisplayTime = 4;          % Time face is displayed
    TaskParameters.TimeResponse = 16;            % Max time allowed for a response
    TaskParameters.NumTrials = 6;                % Num of faces to learn per block - 4 per category: WM, WF, BM, BF
    TaskParameters.ISI = 0;                      % Inter-stimulus interval - in effect 1s, takes ~100 ms to load the picture
    TaskParameters.BaselineTime = 16;            % Duration of the baseline 
    TaskParameters.NumTrialsResp = TaskParameters.NumTrials;
    TaskParameters.MaintainTime = TaskParameters.BaselineTime;
    if strcmp(Answer.Type,'Task');
        load ([ppath filesep 'BlockParameters_3Cond.mat'])
    elseif strcmp(Answer.Type,'Practice');
        load ([ppath filesep 'BlockParameters_3Cond_Practice.mat'])
    end
end


if strcmp(Answer.Parameters,'TIDES-Behv');
    TaskParameters.FaceDisplayTime = 2;          % Time face is displayed
    TaskParameters.TimeResponse = 8;            % Max time allowed for a response
    TaskParameters.NumTrials = 16;               % Num of faces to learn per block - 4 per category: WM, WF, BM, BF
    TaskParameters.ISI = 0.05;                      % Inter-stimulus interval - in effect 1s, takes ~100 ms to load the picture
    TaskParameters.BaselineTime = 5;             % Duration of the baseline - 5s to match rump up and rump down - during rump down do we do a filler task (?)
    TaskParameters.NumTrialsResp = TaskParameters.NumTrials;
    TaskParameters.MaintainTime = 40;
    TaskParameters.TimeConfidence = 3;
    if strcmp(Answer.Type,'Task');
        load ([ppath filesep 'BlockParameters_TIDES-behv.mat'])
    elseif strcmp(Answer.Type,'Practice');
        load ([ppath filesep 'BlockParameters_TIDES-behv_Practice.mat'])
    end
end


end