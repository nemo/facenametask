function [OrderFaceType, StructBlock, AssociationsProbed, OrderResp]= BlockParametersPractice(SubjectInfo, TaskParameters, StructBlockParameters, block)
% Select the order each Face Type will be shown

if TaskParameters.NumTrials/4
    Numtrials_generator = TaskParameters.NumTrials + (4 - rem(TaskParameters.NumTrials,4));
else
    Numtrials_generator = TaskParameters.NumTrials;
end

OrderFaceType_tmp = repmat(1:4,[1 Numtrials_generator/4]);
pass=0;
while pass<1  % Ensure no more than 3 of the same type in a row
    OrderFaceType_tmp = OrderFaceType_tmp(randperm(numel(OrderFaceType_tmp)));
    seqones = findseq(OrderFaceType_tmp);
    oneseq=find(seqones(:,1)==1);
    maxseqones=max(seqones(oneseq,4));
    if maxseqones<3
        pass=1;
    end;
end

StructBlock = [StructBlockParameters(find([StructBlockParameters.Block] == block))];

%clear StructBlockParameters
% (strcmp(SubjectInfo.Study, 'Part-the-Cloud') &&  (SubjectInfo.Session==2))

OrderFaceType=OrderFaceType_tmp(1:TaskParameters.NumTrials)*100;


Order_idx = [StructBlock(find([StructBlock.FaceType]==1))];
Order_idx = [Order_idx.idx];
if (strcmp(SubjectInfo.Study, 'Part-the-Cloud') &&  (SubjectInfo.Session==2))
    Order_idx =  Order_idx(3:end);
end
OrderFaceType(OrderFaceType==100)=Order_idx(1:length( OrderFaceType(OrderFaceType==100)));
clear Order_idx

Order_idx = [StructBlock(find([StructBlock.FaceType]==2))];
Order_idx = [Order_idx.idx];
if (strcmp(SubjectInfo.Study, 'Part-the-Cloud') &&  (SubjectInfo.Session==2))
    Order_idx =  Order_idx(3:end);
end
OrderFaceType(OrderFaceType==200)=Order_idx(1:length( OrderFaceType(OrderFaceType==200)));
clear Order_idx

Order_idx = [StructBlock(find([StructBlock.FaceType]==3))];
Order_idx = [Order_idx.idx];
if (strcmp(SubjectInfo.Study, 'Part-the-Cloud') &&  (SubjectInfo.Session==2))
    Order_idx =  Order_idx(3:end);
end
OrderFaceType(OrderFaceType==300)=Order_idx(1:length( OrderFaceType(OrderFaceType==300)));
clear Order_idx

Order_idx = [StructBlock(find([StructBlock.FaceType]==4))];
Order_idx = [Order_idx.idx];
if (strcmp(SubjectInfo.Study, 'Part-the-Cloud') &&  (SubjectInfo.Session==2))
    Order_idx =  Order_idx(3:end);
end
OrderFaceType(OrderFaceType==400)=Order_idx(1:length( OrderFaceType(OrderFaceType==400)));
clear Order_idx OrderFaceType_tmp




AssociationsProbed = [sort(datasample(OrderFaceType(1:TaskParameters.NumTrials),TaskParameters.NumTrials,'Replace',false))];
if (strcmp(SubjectInfo.Study, 'Part-the-Cloud'))
    save([SubjectInfo.ParamFolder filesep 'Parameters_S' SubjectInfo.Subject '_Session_' num2str(SubjectInfo.Session) '_Block_' num2str(block) '.mat'],'AssociationsProbed','OrderFaceType','StructBlock');
elseif (strcmp(SubjectInfo.Study, 'Practice'))
    save([SubjectInfo.SessionFolder filesep 'Parameters_S' SubjectInfo.Subject '_Session_' num2str(SubjectInfo.Session) '_Block_' num2str(block) '.mat'],'AssociationsProbed','OrderFaceType','StructBlock');
else
    save([SubjectInfo.ParamFolder filesep 'Parameters_S' SubjectInfo.Subject '_Block_' num2str(block) '.mat'],'AssociationsProbed','OrderFaceType','StructBlock');
end


if (strcmp(SubjectInfo.Study, 'Part-the-Cloud'))
    load([SubjectInfo.ParamFolder filesep 'Parameters_S' SubjectInfo.Subject '_Session_' num2str(SubjectInfo.Session) '_Block_' num2str(block) '.mat'],'AssociationsProbed','OrderFaceType','StructBlock');
elseif (strcmp(SubjectInfo.Study, 'Practice'))
     load([SubjectInfo.SessionFolder filesep 'Parameters_S' SubjectInfo.Subject '_Session_' num2str(SubjectInfo.Session) '_Block_' num2str(block) '.mat'],'AssociationsProbed','OrderFaceType','StructBlock');
else
    load([SubjectInfo.ParamFolder filesep 'Parameters_S' SubjectInfo.Subject '_Block_' num2str(block) '.mat'],'AssociationsProbed','OrderFaceType','StructBlock');
end


%% Generate a new order to present - but make sure that this is split in half

a=[TaskParameters.NumTrials/2+1:1:TaskParameters.NumTrials];
Order_tmp = [(randperm(TaskParameters.NumTrials/2)) a(randperm(numel(a)))];
OrderFaceType = OrderFaceType(Order_tmp);

clear Order_tmp a

%% Select the order each face will be presented in the response part

pass=0;
while pass<1
    OrderResp = OrderFaceType(randperm(numel(OrderFaceType)));
    if (OrderResp(1)~= OrderFaceType(end)) && (OrderResp(1)~= OrderFaceType(end-1))
        pass=1;
    end
end


%% Select Foils for each face

Mfoils = [StructBlock(find([StructBlock.FaceType]<3))];
Ffoils = [StructBlock(find([StructBlock.FaceType]>2))];

tmp = [1:1:length(Mfoils)];
pass = 0;
while pass<1
    orderTochoose = [randperm(length(Mfoils)); randperm(length(Mfoils))];
    if (any([tmp-orderTochoose(1,:)]==0)) || (any([tmp-orderTochoose(2,:)]==0)) || (any([orderTochoose(1,:)-orderTochoose(2,:)]==0))
        pass =0;
    else
        pass=1;
    end
end

counterM=1;
counterF=1;
for ii=1:length(StructBlock)
    if ([StructBlock(ii).FaceType]<3)
        StructBlock(ii).FoilName1 = [Mfoils(orderTochoose(1,counterM)).Name];
        StructBlock(ii).FoilName2 = [Mfoils(orderTochoose(2,counterM)).Name];
        counterM=counterM+1;
    else
        StructBlock(ii).FoilName1 = [Ffoils(orderTochoose(1,counterF)).Name];
        StructBlock(ii).FoilName2 = [Ffoils(orderTochoose(2,counterF)).Name];
        counterF=counterF+1;
    end
end

select = ismember([StructBlock.idx],OrderFaceType); % this is for the case where the number of trials is less than 16!
if sum(select)~=length(select)
    StructBlock = StructBlock(select);
end
