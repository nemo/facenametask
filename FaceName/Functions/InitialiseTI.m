function [stimulator] = InitialiseTI(port)
%% Function for TI stimulation

serialPort = port;  % Type here the one that says arduino
serialRate = 115200;
stimulator=serial(serialPort,'BaudRate',serialRate);
connectStatus = 0;

% connect
fopen(stimulator);
pause(1)
% check connection
status = stimulator.Status;
if  strcmp(status,'closed')
    disp('* Conneciton failed, check port!');
elseif  strcmp(status,'open')
    disp(['* TI stimulator is connected (port = ',serialPort,', BaudRate = ',num2str(serialRate),')']);
    connectStatus = 1;
end

end