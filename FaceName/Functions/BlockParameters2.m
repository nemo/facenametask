function [StructBlock, AssociationsProbed]= BlockParameters2(SubjectInfo, TaskParameters, StructBlockParameters, block, session)


    %% Generate a random order to present - ensure no more than 2 of the same type in a row
     StructBlock = [StructBlockParameters(find([StructBlockParameters.Block] == block))];

    OrderFaceType_tmp = [StructBlock.FaceType];
        pass=0;
        while pass<1  % Ensure no more than 2 of the same type in a row
            
            order = randperm(numel(OrderFaceType_tmp));
            %OrderFaceType_tmp = OrderFaceType_tmp(randperm(numel(OrderFaceType_tmp)));
            seqones = findseq(OrderFaceType_tmp(order));
            oneseq=find(seqones(:,1)==1);
            maxseqones=max(seqones(oneseq,3));
            if isempty(maxseqones)
                pass = 1;
            end
            if maxseqones<3
                pass=1;
            end;
        end
    
    OrderFaceType = OrderFaceType_tmp(order);
    StructBlock = struct2table(StructBlock);
    StructBlock = StructBlock(order,:); % now the table has the new order 
    StructBlock.idx = [];
    StructBlock.idx = [1:height(StructBlock)]'; % idx is now trial order
    
    
    clear OrderFaceType_tmp
    
    %% Select the order each face will be presented in the response part
    ntrials = 1:numel(OrderFaceType);
    pass=0;
    while pass<1
        OrderResp = ntrials(randperm(numel(ntrials)));
        y = intersect(OrderResp(end-1:end),ntrials(1:2)); % last two encode cannot be the 1st or 2nd to recall
        if isempty(y) 
            pass=1;
        end
    end
    
    StructBlock.RespOrder = OrderResp'; % OrderResp added to table
   
   
    %% Select Foils for each face
    
    StructBlock.FaceType2 = mod(StructBlock.FaceType,2); % now males = 1 and females = 0
    M = StructBlock(StructBlock.FaceType2==1,:);
    F = StructBlock(StructBlock.FaceType2==0,:);
    
    % M foils
    pass=0;
    while pass<1
        foilnames1 = randperm(height(M));
        foilnames2 = randperm(height(M));
        if any(strcmpi(M.Name,M.Name(foilnames1))) || any(strcmpi(M.Name,M.Name(foilnames2))) || any(strcmpi(M.Name(foilnames1),M.Name(foilnames2)))
            pass=0;
        else
            pass=1;
        end
    end
    
    M.FoilName1=M.Name(foilnames1);
    M.FoilName2=M.Name(foilnames2);

    % F foils
    pass=0;
    while pass<1
        foilnames1 = randperm(height(F));
        foilnames2 = randperm(height(F));
        if any(strcmpi(F.Name,F.Name(foilnames1))) || any(strcmpi(F.Name,F.Name(foilnames2))) || any(strcmpi(F.Name(foilnames1),F.Name(foilnames2)))
            pass=0;
        else
            pass=1;
        end
    end

    F.FoilName1=F.Name(foilnames1);
    F.FoilName2=F.Name(foilnames2);
    
    StructBlockcomp = vertcat(F,M);
    StructBlockcomp = sortrows(StructBlockcomp,'idx','ascend');
    StructBlock = table2struct(StructBlockcomp);
    
       
end

