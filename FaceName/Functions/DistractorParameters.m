function [StructDistractor] = DistractorParameters(TaskParameters, NumDuration)
%% Odd/even distractor parameter generator


NumStimuli = TaskParameters.MaintainTime*NumDuration; % larger than what would be shown

% Generate ISI distribution
meanISI = 2.1;
sdISI = 0.5;
ISI_dist = sdISI.*randn(NumStimuli,1) + meanISI;

% Generate stim distribution - numbers might be repeated, but have similar
% number of odd and even numbers; allow a difference of 30%

 pass=0;
    while pass<1
        stimuli = randi(99,1,NumStimuli);
        if ~(numel(find(mod(stimuli,2)==0)) > NumStimuli*0.7)
            pass=1;
        end
    end

% Create a structure for the distractor task

StructDistractor = table;
StructDistractor.trial = (1:NumStimuli)';
StructDistractor.Stimuli = stimuli';
StructDistractor.ISI = ISI_dist;
StructDistractor.Type = mod(stimuli,2)';
StructDistractor.Presented = zeros(NumStimuli,1);
StructDistractor = table2struct(StructDistractor);
end

