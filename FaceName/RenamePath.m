function RenamePath(path)
%% Rename Paths to use task in a different computer

NewPath = ([path filesep 'Resources' filesep 'CFDVersion2.0.3' filesep 'CFD2.0.3Images']);

load BlockParameters_3Cond_setup.mat

for ii=1:length(StructBlockParameters)
    StructBlockParameters(ii).FacePath = NewPath;    
end

matfile1 = fullfile(path, 'BlockParameters_3Cond.mat');  
matfile2 = fullfile(path, 'PathTask.mat');  

save (matfile1, 'OrderStim', 'StructBlockParameters');
save (matfile2, 'path');

end


