%% Run the Face-Name task

% January 2018
% v3 - save continuous timestamps
%    - save block info and EVs txt files
% v4 March 2018 - save RT from onset button press
%    - save additional EV txt files in separate directory per run (fsl
%    friendly)
% v5 December 2018 - Changes for UG projects @ Surrey 
%                  - no small blocks - 20 associations in a row for each
%                  block (each block is either real or sham)
%                  - frequency in channel 2 changed to 50Hz
% v6 January 2019 - 3 Stimulation conditions
% v7 July 2019 - adapted for MRI
% v8 June 2021 - adjusting for more flexibility and a Part-the-Cloud
% project
% v9 Jan 2022 - adjust for session based behv task

%% Clear the workspace and the screen
sca;
close all;
clear

rand('state',sum(100*clock));

%% Go to task directory
load (['Parameters' filesep 'PathTask.mat'])
TaskDir = (path);
ResultsDir =  ([TaskDir filesep 'Data']);
addpath(genpath(TaskDir));

cd (TaskDir)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Subject Info, Directories, Task Parameters 
[SubjectInfo, TaskParameters, OrderStim, StructBlockParameters] = GetSubjectInfo(ResultsDir);

%%
MRI = SubjectInfo.MRI;
Stimulation = SubjectInfo.Stimulation;

if (Stimulation)
    port = SubjectInfo.COM;
    % Standard Parameters to start with
    rampOnTime = 5*1000;        % ms   %% Cannot be longer than baseline time
    rampOffTime =5*1000;        % ms

    % Channel 1
    Freq1 = 2000;          % Hz
    Amp1 = 2;             % mA
    load1a = '32';
    load1b = '32';
    pin1a = '01';
    pin1b = '02';
    
    % Channel 2
    Freq2 = 2005;          % Hz
    Amp2 = 2;              % mA
    load2a = '32';
    load2b = '32';
    pin2a = '03';
    pin2b = '04';
    % Initialise TI
    [stimulator] = InitialiseTI(port);
    % Give some initial parameters
    UploadStimTI(stimulator, Freq1, Freq2, Amp1, Amp2, load1a, load1b, load2a, load2b, pin1a, pin1b, pin2a, pin2b)
    stimulationStatus = 0;
end


%% Screen Parameters

% Enable alpha blending with proper blend-function
AssertOpenGL;

% Check screens to see if dual display is possible
screens=Screen('Screens');
screenNumber=max(screens);

Screen('Preference', 'SkipSyncTests', 2)  % Change to zero for actual experiment
white=WhiteIndex(screenNumber);
black=BlackIndex(screenNumber);

[window screenRect]=Screen('OpenWindow',screenNumber, white);
%[window, screenRect]=Screen('OpenWindow',screenNumber,white, [ 0 0 screenSize(3)/2 screenSize(4)/2]);
Screen('BlendFunction', window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% Get the size of the on screen window
[screenXpixels, screenYpixels] = Screen('WindowSize', window);

% Query the frame duration
ifi = Screen('GetFlipInterval', window);

% Get the centre coordinate of the window
[xCenter, yCenter] = RectCenter(screenRect);


%% Make Response Rects

% Define task colours
red = [255 0 0];
blue = [0 153 153];
grey = [128 128 128];

% Make a base Rect
%%imageRect = [0 0 250 100];
imageRect = [0 0 310 100];
%%imageRect = [0 0 360 100]; - ICL

% Screen X positions of rectangles
%squareXposTop = [screenXpixels * 0.25  screenXpixels * 0.5  screenXpixels * 0.75];
squareXposTop = [screenXpixels * 0.15  screenXpixels * 0.5  screenXpixels * 0.85];
squareXposMid = [screenXpixels * 0.375  screenXpixels *  0.625 ];

% Make rectangle coordinates
TopRects = nan(4, 3);
for i = 1:size(TopRects,2)
    TopRects(:, i) = CenterRectOnPointd(imageRect, squareXposTop(i), yCenter+100);
end

clear i
MidRects = nan(4, 2);
for i = 1:size(MidRects,2)
    MidRects(:, i) = CenterRectOnPointd(imageRect, squareXposMid(i), yCenter+300);
end

%BottomRect = CenterRectOnPointd(imageRect, xCenter, yCenter+360);

allRects = [TopRects MidRects];

% Screen X positions of rectangles
squareXposTop = [screenXpixels * 0.25  screenXpixels * 0.5  screenXpixels * 0.75];
squareXposMid = [screenXpixels * 0.375  screenXpixels *  0.625 ];

% Make rectangle coordinates
TopRects = nan(4, 3);
for i = 1:size(TopRects,2)
    TopRects(:, i) = CenterRectOnPointd(imageRect, squareXposTop(i), yCenter+100);
end

clear i
MidRects = nan(4, 2);
for i = 1:size(MidRects,2)
    MidRects(:, i) = CenterRectOnPointd(imageRect, squareXposMid(i), yCenter+300);
end

%BottomRect = CenterRectOnPointd(imageRect, xCenter, yCenter+360);


if (strcmp(SubjectInfo.Study, 'Part-the-Cloud'))
    allRects = [TopRects];
else
    allRects = [TopRects MidRects];
end

screenWidth = screenRect(3);
screenHeight = screenRect(4);
x0 = screenWidth/2;
y0 = screenHeight/2;

%% Define Squares for confidence

% Make a base Rect of 200 by 200 pixels
baseRect = [0 0 200 200];

% Screen X positions of our three rectangles
squareXpos = [screenXpixels * 0.2  screenXpixels * 0.4  screenXpixels * 0.6 screenXpixels * 0.8 ];
numSquares = length(squareXpos);

% Make our rectangle coordinates
allSquares = nan(4, 3);
for i = 1:numSquares
    allSquares(:, i) = CenterRectOnPointd(baseRect, squareXpos(i), yCenter);
end

% Sync us and get a time stamp
vbl = Screen('Flip', window);
waitframes = 1;

% Maximum priority level
topPriorityLevel = MaxPriority(window);
Priority(topPriorityLevel);


%% Keyboard Parameters
deviceIndex = [];
KbName('UnifyKeyNames');
ListenChar(-1); %makes it so characters typed don?t show up in the command window  - remove if testing

if (MRI)
    
    if strcmp(SubjectInfo.MRIsite,'CIF')
        escapeKey = KbName('q');
        respKey = KbName('d');
        leftKey = KbName('b');
        rightKey = KbName('c');
        tkey=KbName('t');
    end
    
    if strcmp(SubjectInfo.MRIsite,'Invicro')
        escapeKey = KbName('q');
        respKey = KbName('1!');
        leftKey = KbName('b');
        rightKey = KbName('2@');
        tkey=KbName('t');
        
    end
    
else
    escapeKey = KbName('q');
    %escapeKey = KbName('ESCAPE');
    respKey = KbName('SPACE');
    leftKey = KbName('LeftArrow');
    rightKey = KbName('RightArrow');
end

keysOfInterest=zeros(1,256);
keysOfInterest(rightKey)=1;
keysOfInterest(leftKey)=1;
keysOfInterest(respKey)=1;

if (MRI)
    keysOfInterest(tkey)=1;
end

%% Mouse
% Here we set the initial position of the mouse to be in the centre of the
% screen
Mouse = false;
if (Mouse)
    SetMouse(xCenter, yCenter, window);
    offsetSet = 0;
end


%% Text Parameters
TextSize = 45;
Screen('TextFont', window, 'Helvetica');
Screen('TextSize', window, TextSize);


%% Fixation Cross Parameters
fixCrossDimPix = 50;
lineWidthPix = 7;

xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
allCoords = [xCoords; yCoords];

%% Variables to Save

StructTask.block = [];          % Block number
StructTask.trial = [];          % Trial number (1:1:Numtrials)
StructTask.StimType = [];       % Type of stimulation; 0 = sham; 1 = TI
StructTask.OrderCond = [];      % 1 = BMfaces ; 2 = WMfaces ;  3 = BFfaces  ; 4 = WFfaces
StructTask.TrialStart = [];     % Trial start time in relation to the beginning of the task
StructTask.FacePresented = [];  % Time at which the face was presented in relation to the beginning of the task
StructTask.Face = [];           % Full path to the face picture
StructTask.Name = [];           % Name
%StructTask.NameNum = [];        % Number matching the name
%StructTask.NameFreqOrder = [];  % The order the name has in terms of frequency (small numbers are more frequent)
%StructTask.NameFreq = [];       % The actual number of newborns with that name (larger numbers are more frequent)
StructTask.RespOrder = [];      % Order in which this image will be presented during response period
StructTask.RespTimeStart = [];  % Time the response time started for each trial in relation to the beginning of the task
StructTask.OnsetRespButtonPress = [];  % Onset button press 
StructTask.NameFoil1 = [];       % Names of all foils presented for that trial ordered for their rect position - foils come from names that were presented in the block
StructTask.NameFoil2 = [];       % Names of all foils presented for that trial ordered for their rect position - foils come from names that were presented in the block
StructTask.NameDistractor1 = []; % Names of all distractors presented for that trial ordered for their rect position - distractors come from names that were not presented in the block
StructTask.NameDistractor2 = []; % Names of all distractors presented for that trial ordered for their rect position - distractors come from names that were not presented in the block
StructTask.DistractorNameFreq1 = [];  % The actual number of newborns with that name (larger numbers are more frequent)
StructTask.DistractorNameFreq2 = [];  % The actual number of newborns with that name (larger numbers are more frequent)
StructTask.RectFoil = [];       % Positions of the foils in the response rectangles
StructTask.RectDistractor = []; % Positions of the distractors in the response rectangles
StructTask.RectChosen = [];     % Position of the rectangle selected by the subject (from 1:9)
StructTask.Accuracy = [];       % Accuracy; 1 = correct; 0 = incorrect; NaN = omission
StructTask.Foil = [];           % Foil = NaN if no foil was pressed; 1 = foil was pressed
StructTask.Distractor = [];     % Distractor = NaN if no distractor was pressed; 1 = distractor was pressed
StructTask.RT = [];             % RT, NaN = omission
StructTask.RTonset = [];        % RT since onset of button press
StructTask.rpress = [];         % Number of button presses to the right
StructTask.lpress = [];         % Number of button presses to the left
StructTask.ConfidenceStartTime = [];  % Confidence start time
StructTask.Confidence = [];     % Confidence rating
StructTask.ConfidenceRT = [];   % RT for Confidence rating
StructTask.Probed = [];         % Indicates whether this is one of the selected proned associations for this block
StructTask.Counting = [];       % Task counter

%% Variables to save for EVs
EVblock = [];


%% Define the order of the blocks
blockOrder = (regexp (SubjectInfo.Block, ' ', 'split'));

session = SubjectInfo.Session;
block = SubjectInfo.Block;
block = str2num(block);
subject = str2num(SubjectInfo.Subject);
run = SubjectInfo.Run;

bb = 1;
counter = 0;
counterdisttask=1;
HideCursor;


%% ------- Task Loop -------------
% --------------------------------
TextSize = 60;  %%%%%%%%%%%%%
Screen('TextSize', window, TextSize);  %%%%%%%%%%%%%%%%%
DrawFormattedText(window, 'Face-Name Task', 'center', 'center', [0 0 0]);
DrawFormattedText(window, 'Get Ready!', 'center', yCenter+100, [0 0 0]);
Screen(window, 'Flip');
 
 if MRI == 1
     [keyisdown,secs,keycode] = KbCheck;
     while(~keycode(tkey))       % wait for a right button press
         [keyisdown,secs,keycode] = KbCheck;
         WaitSecs(0.001); % delay to prevent CPU hogging
     end
 else
     WaitSecs(2)
 end
 
 counter_ev = 1;
 TimeTaskStart = GetSecs(); 
 

for bb=1:numel(block)
    
    %% Load Task Parameters file for the Block
    if strcmp(SubjectInfo.Study,'TIDES-Behv')
        StructBlockParameters = [StructBlockParameters(find([StructBlockParameters.Session] == session))];
        [StructBlock]= BlockParameters2(SubjectInfo, TaskParameters, StructBlockParameters, block(bb), session);
    elseif strcmp(SubjectInfo.Study,'Part-the-Cloud')
        if strcmp(SubjectInfo.Type,'Task')
            [OrderFaceType, StructBlock, AssociationsProbed, OrderResp]=  BlockParameters(SubjectInfo, TaskParameters, StructBlockParameters, block(bb));
        elseif strcmp(SubjectInfo.Type,'Practice')
            [OrderFaceType, StructBlock, AssociationsProbed, OrderResp]=  BlockParametersPractice(SubjectInfo, TaskParameters, StructBlockParameters, block(bb));
        end
    else
        [OrderFaceType, StructBlock, AssociationsProbed, OrderResp]=  BlockParameters(SubjectInfo, TaskParameters, StructBlockParameters, block(bb));
    end


    %% Start with Rest
    Screen('DrawLines', window, allCoords,lineWidthPix, black, [xCenter yCenter], 2);
    Screen('Flip',window);
    time_start_baseline1 = GetSecs() - TimeTaskStart;
    EVblock(counter_ev).block = bb;
    EVblock(counter_ev).type = 'baseline';
    EVblock(counter_ev).start = time_start_baseline1;
    
    if (Stimulation)
        WaitSecs(TaskParameters.BaselineTime-(rampOnTime/1000))  % Because there is always a ramp
    else
        WaitSecs(TaskParameters.BaselineTime)
    end
    
    %% Define Stimulation Parameters and start stimulation 
    
    if(Stimulation)
        if (SubjectInfo.Encode ==1) % Only stimulate if session includes encode
            if strcmp(SubjectInfo.Study, 'TIDES-MRI')
                if (OrderStim(subject,block(bb))==2) % Steer condition
                    Amp1 = 1;
                    Amp2 = 3;
                else
                    Amp1 = 2;
                    Amp2 = Amp1;
                end
                stimulationStatus = 1;
                StartStimulationTI(stimulator, Amp1, Amp2, rampOnTime)
                
                WaitSecs(rampOnTime/1000)
                
                if(OrderStim(subject,block(bb)))==0
                    % Stop Stimulation
                    StopStimulationTI(stimulator, Amp1, Amp2, rampOffTime)
                    stimulationStatus = 0;
                end
                
            elseif strcmp(SubjectInfo.Study, 'TIDES-Behv') % Here we stimulate per session
                Amp1 = 1;
                Amp2 = 3;
                
                % Only the first block of a series triggers the beginning of stimulation / end
                if bb==1
                    stimulationStatus = 1;
                    StartStimulationTI(stimulator, Amp1, Amp2, rampOnTime)
                    WaitSecs(rampOnTime/1000)
                    
                    if (OrderStim(subject,session)==0)
                        % Stop Stimulation % Sham condition
                        StopStimulationTI(stimulator, Amp1, Amp2, rampOffTime)
                        stimulationStatus = 0;
                    end
                end
            end
        end
    end
    
    
    time_finish_baseline1  = GetSecs() - TimeTaskStart;
    EVblock(counter_ev).end = time_finish_baseline1;
    EVblock(counter_ev).duration = time_finish_baseline1 - time_start_baseline1;
    counter_ev = counter_ev+1;
    
    trial = 1;
    for trial = 1:TaskParameters.NumTrials
         time_start_trial = GetSecs()-TimeTaskStart;
         counter=counter+1;
         if trial==1
             EVblock(counter_ev).block = bb;
             EVblock(counter_ev).type = 'encode';
             EVblock(counter_ev).start = time_start_trial;
             timest = [EVblock(counter_ev).start];
         end

        %% Show stimuli
        if strcmp(SubjectInfo.Study, 'TIDES-Behv')
             tmp_idx = StructBlock(trial);
        else
             tmp_idx = [StructBlock(find([StructBlock.idx]==OrderFaceType(trial)))];
        end
       
        name = tmp_idx.Name;
        stimfilename = ([tmp_idx.FacePath filesep tmp_idx.FaceFolder filesep tmp_idx.FaceFile]);
        imdata = imread(char(stimfilename));
        
        [imageHeight, imageWidth, colorChannels] = size(imdata);
        aspectRatio = imageWidth/imageHeight;
        imageHeights = screenYpixels .* 0.45;
        %imageHeights = screenYpixels .* 0.3;
        imageWidths = imageHeights .* aspectRatio;
        
        xOffset = xCenter-(imageWidths/2);
        yOffset = yCenter - imageHeights;
        imageRect = [xOffset, yOffset, xOffset+imageWidths, yOffset+imageHeights];
        
        if (SubjectInfo.Encode ==1)
            TextSize = 70;  %%%%%%%%%%%%%
            Screen('TextSize', window, TextSize);  %%%%%%%%%%%%%%%%%
            DrawFormattedText(window, char(name), xCenter-(TextSize*2), yCenter+(imageHeights/2)-(TextSize*1.5), [0 0 0]);
            
            tex=Screen('MakeTexture',window,imdata);
            Screen('DrawTexture', window, tex,[],imageRect);
            Screen(window, 'Flip');
            TimeFaceStarted = GetSecs();
            StructTask(counter).FacePresented = GetSecs()-TimeTaskStart;
            
            while (GetSecs - TimeFaceStarted <= TaskParameters.FaceDisplayTime);
                [keyisdown,secs,keycode] = KbCheck;
                if(keycode(escapeKey))
                    Screen('CloseAll');
                    sca;
                    ShowCursor;
                    ListenChar(0);
                    break;
                end;
            end
          
        end
        StructTask(counter).session = session;
        StructTask(counter).block = block(bb);
        StructTask(counter).trial = trial;
        
        if strcmp(SubjectInfo.Study, 'TIDES-Behv')
             StructTask(counter).StimType = OrderStim(subject,session);
        else
            StructTask(counter).StimType = OrderStim(subject,block(bb));
        end
        
        StructTask(counter).OrderCond = tmp_idx.FaceType;
        
        if (SubjectInfo.Encode ==1)
            StructTask(counter).TrialStart = time_start_trial;
        end
        
        StructTask(counter).Face = stimfilename;
        StructTask(counter).Name = name;
        StructTask(counter).NameFreq = tmp_idx.NameFreq;
        
        if ~strcmp(SubjectInfo.Study, 'TIDES-Behv')
            if any(AssociationsProbed==tmp_idx.idx)
                StructTask(counter).Probed = 1;
            else
                StructTask(counter).Probed = 0;
            end
        elseif strcmp(SubjectInfo.Study, 'TIDES-Behv')
            StructTask(counter).Probed = 1;
        end
        
        
        if strcmp(SubjectInfo.Study, 'TIDES-Behv')
             StructTask(counter).RespOrder = tmp_idx.RespOrder;
        else
             StructTask(counter).RespOrder = find(OrderResp==tmp_idx.idx);
        end
        
        StructTask(counter).NameDistractor1 = tmp_idx.DistractorName1;
        StructTask(counter).DistractorNameFreq1 = tmp_idx.DistractorNameFreq1;
        StructTask(counter).NameDistractor2 = tmp_idx.DistractorName2;
        StructTask(counter).DistractorNameFreq2 = tmp_idx.DistractorNameFreq2;
        StructTask(counter).NameFoil1 = tmp_idx.FoilName1;
        StructTask(counter).NameFoil2 = tmp_idx.FoilName2;
        StructTask(counter).Counting = counter;
        
        
        clear stimfilename imdata tmp_idx
        
   %%
        Screen('DrawLines', window, allCoords,lineWidthPix, black, [xCenter yCenter], 2);
        Screen(window, 'Flip');
        WaitSecs(TaskParameters.ISI)

        if trial == TaskParameters.NumTrials
           timeblock2ended = GetSecs()-TimeTaskStart;
            
            EVblock(counter_ev).end = timeblock2ended;
            EVblock(counter_ev).duration = timeblock2ended - timest;
            counter_ev = counter_ev+1;
        end
        if (SubjectInfo.Encode ==1)
            Screen('Close', tex);
            %Screen('Close');
        end
       
    end
    
    %% stop stimulation if per block
    if strcmp(SubjectInfo.Study, 'TIDES-MRI')
        if(Stimulation)
            if(stimulationStatus)
                % Stop Stimulation
                StopStimulationTI(stimulator, Amp1, Amp2, rampOffTime)
                stimulationStatus = 0;
            end
        end 
    end
    
     %% Save Information
    pathname = SubjectInfo.RunFolder;
    matfile = fullfile(pathname, SubjectInfo.baseNameTask);
    
    save (matfile, 'SubjectInfo', 'StructTask', 'TaskParameters', 'EVblock');
    
    %% Maintenance / Distractor Period
    Screen('DrawLines', window, allCoords,lineWidthPix, black, [xCenter yCenter], 2);
    Screen('Flip',window);
    time_start_baseline2 = GetSecs()- TimeTaskStart;
    
    EVblock(counter_ev).block = bb;
    EVblock(counter_ev).type = 'baseline';
    EVblock(counter_ev).start = time_start_baseline2;
    
    if ~strcmp(SubjectInfo.Study, 'TIDES-Behv')
        WaitSecs(TaskParameters.MaintainTime) % just fixation cross 
    elseif strcmp(SubjectInfo.Study, 'TIDES-Behv')
        NumDuration = 0.5;
        if (SubjectInfo.Encode ==1)
            
            % --- Distractor Task ----
            % 1. Say odd << and even >> in the screen for 1 s
            DrawFormattedText(window, 'Odd or Even Task', 'center',  yCenter-100, [0 0 0]);
            DrawFormattedText(window, 'Press LEFT Arrow for ODD Numbers', 'center', yCenter+100, [0 0 0]);
            DrawFormattedText(window, 'Press RIGHT Arrow for EVEN Numbers', 'center', yCenter+200, [0 0 0]);
            Screen(window, 'Flip');
            WaitSecs(4)
            
            % Present Fixation Cross
            Screen('DrawLines', window, allCoords,lineWidthPix, black, [xCenter yCenter], 2);
            Screen(window, 'Flip');
            WaitSecs(TaskParameters.ISI)
            
            %Get Stimuli Parameters
            [StructDistractor] = DistractorParameters(TaskParameters, NumDuration);
            
            % 2. Show odd and even numbers 1:99
            counterdist = 1;
            TextSize = 160;
            Screen('TextSize', window, TextSize);
            
            
            % Initialise keyboard
            keysOfInterest=zeros(1,256);
            keysOfInterest(rightKey)=1;
            keysOfInterest(leftKey)=1;
            keysOfInterest(respKey)=1;
            
            KbQueueCreate(deviceIndex, keysOfInterest);
            KbQueueStart(deviceIndex);
            
            timesUp = 0;
            
            TimeDistStart = GetSecs();
            while (GetSecs - TimeDistStart <= TaskParameters.MaintainTime-4-TaskParameters.ISI);
                
                responded = 0;
                number = [StructDistractor(counterdist).Stimuli];
                DrawFormattedText(window, num2str(number), 'center', 'center', [0 0 0]);
                Screen(window, 'Flip');
                TimePresented = GetSecs();
                counterdisttask = counterdisttask+1;

                StructDistractor(counterdist).Presented = 1;
                StructDistractor(counterdist).TimePresented = GetSecs() - TimeDistStart;
                
                StructTaskDist(counterdisttask).block = bb;
                StructTaskDist(counterdisttask).trial = [StructDistractor(counterdist).trial];
                StructTaskDist(counterdisttask).Stimuli = number;
                StructTaskDist(counterdisttask).Type = [StructDistractor(counterdist).Type];
                
                tic
                while (toc < NumDuration)
                    [pressed, firstPress]=KbQueueCheck(deviceIndex);
                    if pressed
                        responded = 1;
                        StructDistractor(counterdist).RT = GetSecs() - TimePresented;
                        StructTaskDist(counterdisttask).RT = [StructDistractor(counterdist).RT];
                        if firstPress(rightKey)
                            if (StructDistractor(counterdist).Type==0) % Correct answer
                                StructDistractor(counterdist).accuracy = 1;
                                StructTaskDist(counterdisttask).accuracy = 1;
                            else
                                StructDistractor(counterdist).accuracy = 0;
                                StructTaskDist(counterdisttask).accuracy = 0;
                            end
                            
                        elseif firstPress(leftKey)
                            if (StructDistractor(counterdist).Type==1) % Correct answer
                                StructDistractor(counterdist).accuracy = 1;
                                StructTaskDist(counterdisttask).accuracy = 1;
                            else
                                StructDistractor(counterdist).accuracy = 0;
                                StructTaskDist(counterdisttask).accuracy = 0;
                            end
                        end
                    end
                end
                
                % ISI Period -
                % Present Fixation Cross
                Screen('DrawLines', window, allCoords,lineWidthPix, black, [xCenter yCenter], 2);
                Screen(window, 'Flip');
                StructDistractor(counterdist).TimeISIstart = GetSecs() - TimeDistStart;
                StructTaskDist(counterdisttask).ISI = [StructDistractor(counterdist).ISI];
                
                tic
                while (toc < StructDistractor(counterdist).ISI)
                    [pressed, firstPress]=KbQueueCheck(deviceIndex);
                    if pressed
                        if ~(responded) % If already press during number do not take another key press
                            responded = 1;
                            StructDistractor(counterdist).RT = GetSecs() - TimePresented;
                            StructTaskDist(counterdisttask).RT = StructDistractor(counterdist).RT;
                            if firstPress(rightKey)
                                if (StructDistractor(counterdist).Type==0) % Correct answer
                                    StructDistractor(counterdist).accuracy = 1;
                                    StructTaskDist(counterdisttask).accuracy = 1;
                                else
                                    StructDistractor(counterdist).accuracy = 0;
                                    StructTaskDist(counterdisttask).accuracy = 0;
                                end
                                
                            elseif firstPress(leftKey)
                                if (StructDistractor(counterdist).Type==1) % Correct answer
                                    StructDistractor(counterdist).accuracy = 1;
                                    StructTaskDist(counterdisttask).accuracy = 1;
                                else
                                    StructDistractor(counterdist).accuracy = 0;
                                    StructTaskDist(counterdisttask).accuracy = 0;
                                end
                            end
                        end
                    end
                end
                counterdist = counterdist + 1;
            end
        end
    end
        
    %
    EVblock(counter_ev).end = GetSecs()- TimeTaskStart;
    EVblock(counter_ev).duration = [EVblock(counter_ev).end] - time_start_baseline2;
    counter_ev = counter_ev+1;
    
    %% Save Information
    pathname = SubjectInfo.RunFolder;
    matfile = fullfile(pathname, SubjectInfo.baseNameTask);
    
    if ((SubjectInfo.Encode ==1) && strcmp(SubjectInfo.Study, 'TIDES-Behv'))
        save (matfile, 'SubjectInfo', 'StructTask', 'TaskParameters', 'StructDistractor','EVblock','StructTaskDist');
    else
        save (matfile, 'SubjectInfo', 'StructTask', 'TaskParameters', 'EVblock');
    end

    
    %% Collect Responses at the end of each Block
    if strcmp(SubjectInfo.Study, 'TIDES-Behv')
        if (SubjectInfo.Encode ==1)
            TextSize = 60;
            Screen('TextSize', window, TextSize);
            DrawFormattedText(window, 'Recall Faces + Names', 'center',  'center', [0 0 0]);
            Screen(window, 'Flip');
            WaitSecs(2)
        end
    end
    
            
    KbQueueCreate(deviceIndex, keysOfInterest);
    KbQueueStart(deviceIndex);
    block_resp = [StructTask(find([StructTask.block]==block(bb)))];
    
    
    NumTrialsResp = TaskParameters.NumTrialsResp;
    Resp=sort([block_resp.RespOrder]);
        
    if (strcmp(SubjectInfo.Study, 'Part-the-Cloud')) % In this case only 3 response options
        order_resp = [zeros(1,1) 1 (ones(1,1))*3];
    else
        order_resp = [zeros(1,2) 1 (ones(1,2))*3];
    end
     
    EVresponse(bb).StartTime =  GetSecs()-TimeTaskStart;
    
    
    EVblock(counter_ev).block = bb;
    EVblock(counter_ev).type = 'recall';
    EVblock(counter_ev).start = [EVresponse(bb).StartTime];

    for trial = 1:NumTrialsResp
       
        
        rectColor = grey;
        
        tmp_idx = [block_resp(find([block_resp.RespOrder]==Resp(trial)))];
        stimfilename = ([tmp_idx.Face]);
        imdata = imread(char(stimfilename));
        stimname = tmp_idx.Name;
        
        ordernames_resp = order_resp(randperm(numel(order_resp)))';
        
        %% create here an array of names - record the position of the correct name; then if it matches j the correct response was given
        rect = {};
        
        
        if (strcmp(SubjectInfo.Study, 'Part-the-Cloud'))
            rect(find(ordernames_resp==1),:)=cellstr(stimname); % this is the position of the correct name
            rect(find(ordernames_resp==3),:)=[cellstr([tmp_idx.NameDistractor1])];
            rect(find(ordernames_resp==0),:)=[cellstr([tmp_idx.NameFoil1])];
            
        else
            rect(find(ordernames_resp==1),:)=cellstr(stimname); % this is the position of the correct name
            rect(find(ordernames_resp==3),:)=[cellstr([tmp_idx.NameDistractor1]); cellstr([tmp_idx.NameDistractor2])];
            rect(find(ordernames_resp==0),:)=[cellstr([tmp_idx.NameFoil1]); cellstr([tmp_idx.NameFoil2])];
            
        end
        
       
        StructTask(tmp_idx.Counting).RespTimeStart = GetSecs()-TimeTaskStart;
     
        StructTask(tmp_idx.Counting).RectFoil = find(ordernames_resp==0);
        StructTask(tmp_idx.Counting).RectDistractor = find(ordernames_resp==3);
        
         if (strcmp(SubjectInfo.Study, 'Part-the-Cloud'))
             currentsq = 1;
         else
             currentsq = 2;
         end
        
        responded = 0;
        accuracy = NaN;
        distractor = NaN;
        foil = NaN;
        RT = NaN;
        RTonset = NaN;
        resp0_tmp = find(ordernames_resp==0);
        resp3_tmp = find(ordernames_resp==3);
        
        keysOfInterest=zeros(1,256);
        keysOfInterest(rightKey)=1;
        keysOfInterest(leftKey)=1;
        keysOfInterest(respKey)=1;
        
        KbQueueCreate(deviceIndex, keysOfInterest);
        KbQueueStart(deviceIndex);
        
        press1 = 0;
        rpress = 0;
        lpress = 0;
        rtonset = 0;
        
        TimeResponseStart = GetSecs();
        
        while (GetSecs - TimeResponseStart <= TaskParameters.TimeResponse);
            
            % Move square depending on key press
            %[keyisdown,secs,keycode] = KbCheck;
            [pressed, firstPress]=KbQueueCheck(deviceIndex); % so it only does one keypress at a time
            if pressed
                if press1 == 0
                    if rtonset == 0
                        FirstButtonPress = GetSecs();
                        rtonset = 1;
                    end
                    StructTask(tmp_idx.Counting).OnsetRespButtonPress = GetSecs() - TimeTaskStart;
                    press1 = 1;
                end
                if firstPress(rightKey)
                    currentsq = currentsq + 1;
                    rpress = rpress + 1;
                elseif firstPress(leftKey)
                    currentsq = currentsq - 1;
                    lpress = lpress + 1;  
                end
            end
            if currentsq < 1
                currentsq = 1;
            end
            
            if (strcmp(SubjectInfo.Study, 'Part-the-Cloud'))
                if currentsq > size(allRects,2)
                    currentsq = 1;
                end
            else
                 if currentsq > size(allRects,2)
                    currentsq = size(allRects,2);
                end
            end
            
            
            for jj=1:size(allRects,2)
                Screen('FillRect', window, grey, allRects(:,jj));
            end
            Screen('FillRect', window, blue, allRects(:,currentsq));
            
            
            for r = 1:size(allRects,2)
                if r<=3
                    DrawFormattedText(window, char(rect(r,:)), squareXposTop(r)-(TextSize*2), yCenter+120, [1 1 1]);
                elseif r>=4
                    DrawFormattedText(window, char(rect(r,:)), squareXposMid(r-3)-(TextSize*2), yCenter+305, [1 1 1]);
                end
            end
            
            % KbReleaseWait;
            tex=Screen('MakeTexture',window,imdata);
            Screen('DrawTexture', window, tex,[],imageRect);
            Screen('Flip', window)
            
            WaitSecs(0.004)
            %[pressed, firstPress]=KbQueueCheck(deviceIndex);
            if pressed
                %[keyisdown,secs,keycode] = KbCheck;
                if firstPress(respKey)
                    if  rtonset == 1  
                        RTonset = GetSecs() - FirstButtonPress; % For the cases where only the respKey was pressed
                    else
                        RTonset = GetSecs() - TimeResponseStart;
                    end
                    
                    RT = GetSecs() - TimeResponseStart;
                    responded=currentsq;
                    resp0_tmp = find(ordernames_resp==0);
                    resp3_tmp = find(ordernames_resp==3);
                    if responded == find(ordernames_resp==1)
                        accuracy = 1;      
                    elseif any(responded == resp0_tmp)
                        accuracy = 0;
                        foil = 1;
                        distractor = 0;
                    elseif any(responded == resp3_tmp)
                        accuracy = 0;
                        distractor = 1;
                        foil = 0;
                    end
                end
            end
            
            if responded>0
                Screen('Flip', window, [], 0);
                WaitSecs(0.2);
                break;
            end
            
          [keyisdown,secs,keycode] = KbCheck;
          if(keycode(escapeKey))
              Screen('CloseAll');
              sca;
              ShowCursor;
              ListenChar(0);
              break;
          end;
          
        end
        KbQueueRelease(deviceIndex);
        %KbQueueStop(deviceIndex)
        StructTask(tmp_idx.Counting).RectChosen = responded;
        StructTask(tmp_idx.Counting).Accuracy = accuracy;
        StructTask(tmp_idx.Counting).Foil = foil;
        StructTask(tmp_idx.Counting).Distractor = distractor;
        StructTask(tmp_idx.Counting).RTonset = RTonset;
        StructTask(tmp_idx.Counting).RT = RT;
        StructTask(tmp_idx.Counting).rpress = rpress;
        StructTask(tmp_idx.Counting).lpress = lpress;
        Screen('Close', tex);
        Screen('Close');

        
        %% Confidence
        if ~(strcmp(SubjectInfo.Study, 'Part-the-Cloud'))
            if (SubjectInfo.Encode ==1) % Only present with encoding blocks
                keysOfInterest=zeros(1,256);
                keysOfInterest(rightKey)=1;
                keysOfInterest(leftKey)=1;
                keysOfInterest(respKey)=1;
                
                KbQueueCreate(deviceIndex, keysOfInterest);
                KbQueueStart(deviceIndex);
                responded = 0;
                currentsq = 1;
                
                TimeConfidenceStart = GetSecs();
                StructTask(tmp_idx.Counting).ConfidenceStartTime = GetSecs - TimeTaskStart;
                
                while (GetSecs - TimeConfidenceStart <= TaskParameters.TimeConfidence);
                    
                    
                    % Move square depending on key press
                    %[keyisdown,secs,keycode] = KbCheck;
                    [pressed, firstPress]=KbQueueCheck(deviceIndex);
                    if pressed
                        if firstPress(rightKey)
                            currentsq = currentsq + 1;
                        elseif firstPress(leftKey)
                            currentsq = currentsq - 1;
                        end
                    end
                    if currentsq < 1
                        currentsq = 1;
                    end
                    
                    if currentsq > size(allSquares,2)
                        currentsq = size(allSquares,2);
                    end
                    
                    
                    for jj=1:size(allSquares,2)
                        Screen('FillRect', window, grey, allSquares(:,jj));
                    end
                    Screen('FillRect', window, blue, allSquares(:,currentsq));
                    
                    for sq =1:size(allSquares,2)
                        DrawFormattedText(window, num2str(sq), squareXpos(sq)-10, 'center', [1 1 1]);
                    end
                    
                    
                    %vbl  = Screen('Flip', window, vbl + (waitframes - 0.5) * ifi);
                    Screen('Flip', window);
                    if pressed
                        if firstPress(respKey)
                            
                            StructTask(tmp_idx.Counting).ConfidenceRT = GetSecs() - TimeConfidenceStart;
                            responded=currentsq;
                        end
                    end
                    
                    if responded>0
                        Screen('Flip', window, [], 0);
                        WaitSecs(0.2);
                        break;
                    end
                    
                    [keyisdown,secs,keycode] = KbCheck;
                    if(keycode(escapeKey))
                        Screen('CloseAll');
                        sca;
                        ShowCursor;
                        ListenChar(0);
                        break;
                    end;
                    
                    WaitSecs(0.004)
                    
                end
                KbQueueRelease(deviceIndex);
                %KbQueueStop(deviceIndex)
                StructTask(tmp_idx.Counting).Confidence = responded;
                
                clear n stimfilename imdata rect resp0_tmp resp3_tmp tmp_idx
            end
        end
    end
    Timerespended = GetSecs()-TimeTaskStart;
    EVblock(counter_ev).end = Timerespended;
    EVblock(counter_ev).duration = Timerespended - [EVresponse(bb).StartTime];
    counter_ev = counter_ev+1;
    
    Screen('Close');
    
    %% Save Information
    pathname = SubjectInfo.RunFolder;
    matfile = fullfile(pathname, SubjectInfo.baseNameTask);
    
    if ((SubjectInfo.Encode ==1) && strcmp(SubjectInfo.Study, 'TIDES-Behv'))
        save (matfile, 'SubjectInfo', 'StructTask', 'TaskParameters', 'StructDistractor','EVblock','StructTaskDist');
    else
        save (matfile, 'SubjectInfo', 'StructTask', 'TaskParameters', 'EVblock');
    end
    
    bb=bb+1;
    
    
    % Adjust Time
    if MRI == 1
        [keyisdown,secs,keycode] = KbCheck;
        while(~keycode(tkey))       % wait for a right button press
            [keyisdown,secs,keycode] = KbCheck;
            WaitSecs(0.001); % delay to prevent CPU hogging
        end
    end
    
      
end

Screen('DrawLines', window, allCoords,lineWidthPix, black, [xCenter yCenter], 2);
Screen('Flip',window);

if(Stimulation)
    if strcmp(SubjectInfo.Study, 'TIDES-Behv') % Here we stimulate per session
        Amp1 = 1;
        Amp2 = 3;
        if (OrderStim(subject,session)==0) % Sham session; ramp up at the end
            stimulationStatus = 1;
            StartStimulationTI(stimulator, Amp1, Amp2, rampOnTime)
            WaitSecs(rampOnTime/1000)
        end
    elseif (OrderStim(subject,session)==1)
        WaitSecs(rampOnTime/1000)
    end
end

%% If any stimulation is going stop it now
if(Stimulation)
    if(stimulationStatus)
        % Stop Stimulation
        StopStimulationTI(stimulator, Amp1, Amp2, rampOffTime)
        stimulationStatus = 0;
    end
end
WaitSecs(TaskParameters.BaselineTime)


%% Save Information
pathname = SubjectInfo.RunFolder;
matfile = fullfile(pathname, SubjectInfo.baseNameTask);  

if ((SubjectInfo.Encode ==1) && strcmp(SubjectInfo.Study, 'TIDES-Behv'))
    save (matfile, 'SubjectInfo', 'StructTask', 'TaskParameters', 'StructDistractor','EVblock','StructTaskDist');
else
    save (matfile, 'SubjectInfo', 'StructTask', 'TaskParameters', 'EVblock');
end

DrawFormattedText(window, 'Thank you', 'center', 'center', [0 0 0]);
Screen(window, 'Flip');
WaitSecs(2)
Screen('CloseAll');
sca;
ShowCursor;
ListenChar(0);

cd (TaskDir)

%% CLOSE COMMUNICATION WITH DEVICE
if(Stimulation)
    DisconnectTI(stimulator)
end

%% REMIND EXPERIMENTER WHICH BLOCK WAS JUST RUN
clc
warning(['Completed: Block no. ' num2str(block) ', Run no. ' num2str(run)])

% Tell experimenter accuracy if Practice run
if strcmp(SubjectInfo.Study,'TIDES-Behv')
    if strcmp(SubjectInfo.Type,'Practice')
        Accuracy_FaceName = (nansum([StructTask.Accuracy])/length(StructTask))*100
        Accuracy_Distractor =  (nansum([StructDistractor.accuracy])/sum([StructDistractor.Presented]==1))*100 
    end
end
