# FaceNameTask

Face-name association task. 
The task contains encode and recall blocks. During encode blocks, face-name associations will be presented. In the recall period each face is presented with names to be chosen from (target, distractor(s) and foil(s)). After each name choice participants have to indicate how confident they are in their reponse (1 to 4, least to highest confidence, respectively).
There are up to 9 blocks per session (2 sessions) and it is currently coded for 24 participants (such that stimulation conditions are balanced across blocks and participants).

# Requirements:
- MATLAB
- [Psycthtoolbox](http://psychtoolbox.org/) - follow the installation instructions in the psychtoolbox page
- [Chicago Face Database](https://chicagofaces.org/) - unzip the CFDVersion folder add it to the "Resources" directory
- [Arduino](https://www.arduino.cc/) - needed to run TI

# Installation:
Download the folder (and unzip if needed) to the directory you want to run it.
Inside the FaceName directory (in Matlab) run the RenamePath function. To do that type in the command line:

- ` path = ('The path you want to have the task')`

example:
- ` path = (/Users/name/FaceName)`

or if you are in the directory you want to install type in the command line:

- ` path = pwd;`

Then run:

- ` RenamePath(path)`

This should create a number of files in the directory of interest:

- PathTask.m
- BlockParameters_XX.mat - these have the parameters for different versions of the task

The task should be ready to run now.

# Running the task:
Check example detailed instructions in Instructions folder

If you want to apply brain stimulation, first check the COM port as you will need that info. You can check this in Device Manager.

When asked for it type COM and the number (e.g. COM5)

The following fields cannot be left blank: Subject, block (an integer between 1 and 9 or a sequence of integers 1 2 3 ...), run, session, stimulation (true/false or 1/0)

If you need to quit the task press q .

# Issues, questions
email ines.violante@surrey.ac.uk




